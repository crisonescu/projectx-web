<?php

/**
 * File to handle all API requests
 * Accepts GET and POST
 * 
 * Each request will be identified by TAG
 * Response will be JSON data

  /**
 * check for POST request 
 */
if (isset($_POST['tag']) && $_POST['tag'] != '') {
    // get tag
    $tag = $_POST['tag'];

    // include db handler
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();

    // response Array
    $response = array("tag" => $tag, "error" => FALSE);

    // check for tag type
    if ($tag == 'login') {
        // Request type is check Login
        $email = $_POST['email'];
        $password = $_POST['password'];

        // check for user
        $user = $db->getUserByEmailAndPassword($email, $password);
        if ($user != false) {
            // user found
            $response["error"] = FALSE;
            $response["user"]["uid"] = $user["uid"];
			$response["user"]["ProfilePic"] = $user["ProfilePic"];
            $response["user"]["name"] = $user["name"];
            $response["user"]["email"] = $user["email"];
			$response["user"]["EmailConfirmed"] = $user["EmailConfirmed"];
			$response["user"]["RoleName"] = $user["RoleName"];
			$response["user"]["PrivilegeId"] = $user["PrivilegeId"];
			$response["user"]["StatusName"] = $user["StatusName"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
			
			$uid = (string)$response["user"]["uid"];
			if($uid != null){
				$response2 = array();
				$posts = array();
				
				//get user posts feed
				$result2 = mysql_query("SELECT u.name, u.ProfilePic, r.RoleName, r.PrivilegeId, p.* FROM users u LEFT JOIN roles r ON u.RoleId = r.RoleId LEFT JOIN posts p ON u.uid = p.UserId WHERE u.uid = '$uid'");
				if($result2 != null){
					while($row = mysql_fetch_array($result2)) 
					{ 
					$id = $row['PostId'];
					$name = $row['name'];
					$image = $row['PostImg'];
					$status = $row['Description'];
					$profilePic = $row['ProfilePic'];
					$timeStamp=$row['PostTimestamp']; 
					$url=$row['PostUrl']; 

					$posts[] = array('id'=> $id, 'name'=> $name, 'image'=> $image, 'status'=> $status, 'profilePic'=> $profilePic, 'timeStamp'=> $timeStamp, 'url'=> $url);

					} 

					$response2['feed'] = $posts;
				}
				if($response2 != null){
					$filename = "results_{$uid}.json"; 
					$fp = fopen($filename, 'w');
					fwrite($fp, json_encode($response2));
					fclose($fp);
				}
				
				$response2 = array();
				$posts = array();
				//get all posts
				$result2 = mysql_query("SELECT u.name, u.ProfilePic, r.RoleName, r.PrivilegeId, p.* FROM users u LEFT JOIN roles r ON u.RoleId = r.RoleId LEFT JOIN posts p ON u.uid = p.UserId");
				if($result2 != null){
					while($row = mysql_fetch_array($result2)) 
					{ 
					$id = $row['PostId'];
					$name = $row['name'];
					$image = $row['PostImg'];
					$status = $row['Description'];
					$profilePic = $row['ProfilePic'];
					$timeStamp=$row['PostTimestamp']; 
					$url=$row['PostUrl']; 

					$posts[] = array('id'=> $id, 'name'=> $name, 'image'=> $image, 'status'=> $status, 'profilePic'=> $profilePic, 'timeStamp'=> $timeStamp, 'url'=> $url);

					} 

					$response2['feed'] = $posts;
				}
				if($response2 != null){
					$filename = "results_{$uid}_all.json"; 
					$fp = fopen($filename, 'w');
					fwrite($fp, json_encode($response2));
					fclose($fp);
				}
			}
			
            echo json_encode($response);
        } else {
            // user not found
            // echo json with error = 1
            $response["error"] = TRUE;
            $response["error_msg"] = "Incorrect email or password!";
            echo json_encode($response);
        }
    } else if ($tag == 'register') {
        // Request type is Register new user
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
		$RoleId = $_POST['RoleId'];
		$EmailConfirmed = $_POST['EmailConfirmed'];

        // check if user is already existed
        if ($db->isUserExisted($email)) {
            // user is already existed - error response
            $response["error"] = TRUE;
            $response["error_msg"] = "Exista deja un cont pe adresa aceasta de email..";
            echo json_encode($response);
        } else {
            // store user
            $user = $db->storeUser($name, $email, $password, $RoleId, $EmailConfirmed);
            if ($user) {
                // user stored successfully
                $response["error"] = FALSE;
                $response["uid"] = $user["unique_id"];
                $response["user"]["name"] = $user["name"];
                $response["user"]["email"] = $user["email"];
				$response["user"]["EmailConfirmed"] = $user["EmailConfirmed"];
				$response["user"]["StatusName"] = $user["StatusName"];
                $response["user"]["created_at"] = $user["created_at"];
                $response["user"]["updated_at"] = $user["updated_at"];
                echo json_encode($response);
            } else {
                // user failed to store
                $response["error"] = TRUE;
                $response["error_msg"] = "Error occured in Registration";
                echo json_encode($response);
            }
        }
    } else if ($tag == 'addpost') {
        // Request type is Register new user
		$UserId = $_POST['UserId'];
        $post = $_POST['post'];
		
            // store user
            $user = $db->addPost($UserId, $post);
            if ($user) {
                // user stored successfully
                $response["error"] = FALSE;
				$response["user"]["UserId"] = $user["UserId"];
                $response["user"]["post"] = $user["post"];
                echo json_encode($response);
            } else {
                // user failed to store
                $response["error"] = TRUE;
                $response["error_msg"] = "Error occured while trying to post.";
                echo json_encode($response);
            }
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Parametrul 'tag' nu este recunoscut. Tagu-uri recunoscute 'login' , 'register' si 'addPost'.";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Parametrul 'tag' lipseste cu desavarsire!";
    echo json_encode($response);
}
?>
