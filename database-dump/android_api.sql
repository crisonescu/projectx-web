-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2015 at 09:22 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `android_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `CommentId` int(11) NOT NULL,
  `PostId` varchar(2) NOT NULL,
  `UserId` varchar(2) NOT NULL,
  `Anon` varchar(2) NOT NULL DEFAULT '0',
  `Comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `PostId` int(11) NOT NULL,
  `UserId` varchar(2) NOT NULL,
  `Title` varchar(100) NOT NULL DEFAULT 'Fara Titlu',
  `Description` text NOT NULL,
  `PostImg` varchar(200) NOT NULL,
  `PostUrl` varchar(500) NOT NULL,
  `PostTimestamp` varchar(50) NOT NULL DEFAULT '1403375851930'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`PostId`, `UserId`, `Title`, `Description`, `PostImg`, `PostUrl`, `PostTimestamp`) VALUES
(1, '23', 'Titlu de proba', 'aici am zis sa scriu ceva sa vad cum da..', '', '', '1403375851930'),
(2, '23', 'aici e a doua postare', 'postarea 2 din tabelu de posturi. e tot ceva de proba', '', '', '1403375851930'),
(3, '23', 'am zis s-o pun si pe a 3-a', 'ultima postare de test... adica a 3-a', '', '', '1403375851930'),
(4, '24', 'Fara Titlu', 'postare de pe contu'' asda 1', '', '', '1403375851930'),
(5, '24', 'Fara Titlu', 'a 2-a postare de pe contu'' asda', '', '', '1403375851930'),
(9, '26', 'Fara Titlu', 'afara e o vreme de cacat', '', '', '1403375851930'),
(10, '24', 'Fara Titlu', 'postare 22', '', '', '1403375851930'),
(11, '23', 'Fara Titlu', 'fhhhh', '', '', '1403375851930'),
(12, '23', 'Fara Titlu', 'fhhxhfjf', '', '', '1403375851930'),
(13, '23', 'Fara Titlu', 'test text de proba', '', '', '1403375851930');

-- --------------------------------------------------------

--
-- Table structure for table `privileges`
--

CREATE TABLE IF NOT EXISTS `privileges` (
  `PrivilegeId` int(11) NOT NULL,
  `PrivilegeName` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privileges`
--

INSERT INTO `privileges` (`PrivilegeId`, `PrivilegeName`) VALUES
(1, 'PostContent'),
(2, 'EditContent'),
(3, 'DeleteContent'),
(4, 'PostComment'),
(5, 'EditComment'),
(6, 'DeleteComment'),
(7, 'EditUserRole'),
(8, 'MuteUser'),
(9, 'BanUser'),
(10, 'PostContentInNameOfUser'),
(11, 'EditContentInNameOfUser'),
(12, 'DeleteContentInNameOfUser');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `RoleId` int(11) NOT NULL,
  `RoleName` varchar(50) NOT NULL,
  `PrivilegeId` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleId`, `RoleName`, `PrivilegeId`) VALUES
(1, 'User', '1, 2, 3, 4, 5, 6'),
(2, 'Premium User', '1, 2, 3, 4, 5, 6'),
(3, 'Content Manager', '1, 2, 3, 4, 5, 6, 11, 12'),
(4, 'Moderator', '1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12'),
(5, 'Administrator', '1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12');

-- --------------------------------------------------------

--
-- Table structure for table `userfriends`
--

CREATE TABLE IF NOT EXISTS `userfriends` (
  `UFId` int(11) NOT NULL,
  `Uid` varchar(2) NOT NULL,
  `UserFriends` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `StatusId` varchar(2) NOT NULL,
  `ProfilePic` varchar(200) NOT NULL DEFAULT 'defaultuserpic.jpg',
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `RoleId` varchar(2) NOT NULL,
  `EmailConfirmed` varchar(10) NOT NULL DEFAULT 'no',
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `unique_id`, `StatusId`, `ProfilePic`, `name`, `email`, `RoleId`, `EmailConfirmed`, `encrypted_password`, `salt`, `created_at`, `updated_at`) VALUES
(23, '555f3010da6656.99208880', '1', 'defaultuserpic.jpg', 'Cristian Onescu', 'crisonescu@gmail.com', '5', 'yes', 'MrcYLbEoE3pIdmdhflBI6Aps+e4yNmQyZWUyMjg3', '26d2ee2287', '2015-05-22 16:33:04', NULL),
(24, '55b14978d16e19.77451088', '1', 'defaultuserpic.jpg', 'John Winston', 'asd', '1', 'yes', 'DaGubjI/b2HLVOWQkzdvmSi2SIw5ZWIzNGI2ODZm', '9eb34b686f', '2015-07-23 23:07:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userstatuses`
--

CREATE TABLE IF NOT EXISTS `userstatuses` (
  `StatusId` int(11) NOT NULL,
  `StatusName` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userstatuses`
--

INSERT INTO `userstatuses` (`StatusId`, `StatusName`) VALUES
(1, 'NORMAL'),
(2, 'MUTED'),
(3, 'BANNED');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`CommentId`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`PostId`);

--
-- Indexes for table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`PrivilegeId`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleId`);

--
-- Indexes for table `userfriends`
--
ALTER TABLE `userfriends`
  ADD PRIMARY KEY (`UFId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`), ADD UNIQUE KEY `unique_id` (`unique_id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `userstatuses`
--
ALTER TABLE `userstatuses`
  ADD PRIMARY KEY (`StatusId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `CommentId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `PostId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `privileges`
--
ALTER TABLE `privileges`
  MODIFY `PrivilegeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `userfriends`
--
ALTER TABLE `userfriends`
  MODIFY `UFId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `userstatuses`
--
ALTER TABLE `userstatuses`
  MODIFY `StatusId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
